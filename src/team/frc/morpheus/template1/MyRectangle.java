package team.frc.morpheus.template1;

public class MyRectangle implements IShape {

    // Below this comment, insert your fields

    // remember each constructor arg must have a type and a name
    public MyRectangle(REPLACETHIS, WITH, CONSTRUCTOR, ARGS) {
        // initialize your fields here
    }

    /* Put your implementations of IShape methods below here. Hint: hover over the errors your IDE points to
     * get an idea of what you need to do.
     */

    /***
     * toString is a behavior common to all Java objects that determines how a given object should appear when printed
     * to the screen in text form. The default behavior gives the name of the object's class and its unique ID, which
     * is not very useful to us. So, we'll "override" the default with our own custom behavior. It should appear
     * onscreen as an ordered 4-tuple, e.g. "(0,0,1,1)" for a square located at the origin. Just like adding numbers,
     * you can use + to add strings of text together. Adding a number to a string of text will automatically
     * convert it to a string.
     * @return string representation of a rectangle
     */
    @Override
    public String toString() {
        return "(" + PLEASEFIXTHIS + "," + PLEASEFIXTHIS + "," + PLEASEFIXTHIS + "," + PLEASEFIXTHISTOO + ")";
    }
}
