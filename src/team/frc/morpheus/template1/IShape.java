package team.frc.morpheus.template1;

/***
 * A "contract" for what an object must do in order to comply with the IPoint interface.
 */
public interface IShape {
    /* Interfaces only describe behavior (i.e. methods), and cannot have data (i.e. fields).
     * If an interface requires that an object must have certain data, you can declare a "getter" (a method whose
     * "behavior" is to return the value of that data) as demonstrated below with getX and getY. By convention,
     * a Java getter starts with the word "get". The following is not allowed in the interface itself, but you will
     * likely need it in your implementation of the interface:
    double x;
    double y;
     */

    double getX(); // An IShape must be able to supply its X coordinate
    double getY(); // And its Y coordinate
    double getArea(); // And its area

    /* Additional exercise: try adding a new method below this comment which requires that an IShape must be able
     * to provide its perimeter.
     */
}