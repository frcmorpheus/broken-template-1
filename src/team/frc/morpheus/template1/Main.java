package team.frc.morpheus.template1;

import java.util.ArrayList;

public class Main {
    /***
     * Anything you put inside this method will run when Java starts your program. See testShapes() comment
     * for more on methods.
     * @param args The "argument vector" containing information passed to your program by the operating system.
     *             The first item is the name of the program being run, but the rest is under the control of
     *             whoever started your program (e.g. a user or another program). Nominally, there is no particular
     *             limit to the number of arguments that may be passed, but practically there often is one. Don't plan
     *             for more than 100 or so. The meaning of them is arbitrary - you decide how to interpret them.
     *             Many programs publish a help message or manual showing what parameters they understand. We won't be
     *             using any for this exercise.
     */
    public static void main(String... args) {
        /* A "declaration" tells Java the name and type of something. The most basic types (called primitives) are
        * built-in to Java and can be recognized by their lowercase names. The primitives are byte, short, int, long,
        * float, double, boolean and char. Char holds a single character, like 'a' or 'b'. Boolean holds true or false,
        * and every other primitive type represents a number of some kind with various restrictions.
        *
        * The most important primitives you need to remember are:
        * boolean - represents true or false
        * double - represents a number in "scientific notation" (53-bit (really 52) significand, 11-bit exponent, 1 sign bit)
        * int - represents an integer value between -2^31 and 2^31-1 (roughly +/- 2 billion)
        *
        * If you need to represent very large/small numbers, non-integer rational numbers or irrational numbers, use
        * double. For all other numbers, use int.
        *
        * All other types in Java are uppercase, and refer to "classes", e.g. String. You can create your own classes
        * to extend the types of data Java can operate on.
        * */
        int aNumber; // declare variable "aNumber" whose type is "int"
        double aRationalNumber, anIrrationalNumber; // declare two variables whose types are both double

        /* An "assignment" sets the value of something that has already been declared. The value assigned
         * must be compatible with the declared type. For example, the next line is an error. Examine the error, then
         * comment it out to continue. There are actually three kinds of comments, but for now, just remember these two
         * use double-slash to make Java ignore all text until the end of the line; use slash-star to make Java ignore
         * all text until it finds a star-slash.
         */
        aNumber = 3.14;
        aRationalNumber = 5.0 / 2.0 + 1.0; // you can also assign the result of an expression
        anIrrationalNumber = 3.14159;

        /* You can combine a declaration and assignment into a "definition", like so: */
        double mmPerInch = 25.4; // declares a variable mmPerInch whose type is double and whose value is 25.4

        /* All declarations definitions and assignments so far have been inside a method. Inside methods, the lexical
         * order of declarations, definitions and assignments matters. You can't, for instance, assign a variable above
         * where it has been declared. Examine the error and, to continue, convert the following two lines to a
         * definition or move the declaration above the assignment.
         */
        youCantDoThis = 5;
        int youCantDoThis;

        /* When declaring/defining fields and methods in a class/interface, lexical order doesn't matter; e.g. I can
         * access meaningOfLife even though it is below main(), since it is declared as a field of the class. You still
         * have to be mindful of execution order, though - make sure you assign fields before you access them. Don't
         * worry if this doesn't make sense now; this is something best learnt by trial and error.
         */
        double fortyThree = meaningOfLife + 1;

        /* Here we call a method (defined below). Fix the error to continue. The arguments are not used in this case,
        * so it doesn't matter what you supply so long as it is the right type.
        */
        testShapes();

        // implicit return here, program ends since there is nowhere to return to (see testShapes comment)
    }

    /* Here is a field definition. Method/field definitions in classes (i.e. outside methods) can have additional
     * keywords preceding the type (e.g. private, static) which affect visibility, scope and other properties in
     * various ways. The quickest way to learn what that means is to make everything private and examine which
     * errors occur, if any. By convention, fields go at the top of the class before any methods, but you can put
     * them anywhere in the class, as shown here.
     */
    private static int meaningOfLife = 42;


    /* Here is a method definition. Method definitions have a type and a name like variable/field definitions, but
     * their names are followed by parentheses and braces, and they are not set equal (using =) to an expression.
     * Inside the parentheses you declare the arguments (also called parameters) separated by commas. These are bound
     * variables whose values are known when the method is evaluated, like the "x" in the f(x) you are familiar with
     * from basic algebra - the "x" doesn't represent a specific value; rather, it's a placeholder for any number of
     * values you decide later, e.g. when you evaluate something like f(3) or f(5). A method with zero arguments has
     * parentheses with nothing inside them. You can call a method by stating the method's name followed by parentheses,
     * inside which you can supply values for the argument(s) (e.g. 3 or 5 in the above analogy) if the method
     * requires any.
     *
     * The braces contain the code that will be executed when you invoke the method. By convention, this code is
     * indented another level to distinguish it from its surroundings.
     *
     * In addition to the normal types that fields/variables can have, method definitions have access to an additional
     * "type" called void, which represents the lack of a value - in other words, it denotes that a method returns
     * nothing. Void methods implicitly return at the end of the method body, so no return statement is required, but
     * you can optionally add one to return early.
     */
    public static void testShapes(int uselessParameter, double alsoUseless) {

        /* Here we make a list of shapes. The <> are for specifying "type parameters" for a Java feature called
         * Generics. We'll discuss generics later.
         */
        ArrayList<IShape> shapes = new ArrayList<>();

        shapes.add(new Circle(0, 0, 5));
        shapes.add(new MyRectangle(0, 0, 5, 10));

        /* For all shapes in the list, we print out what kind of shape it is, and Java will automatically use our
         * custom toString to display the Shape's information.
         */

        for (IShape shape : shapes) {
            System.out.println(shape.getClass().getName() + " " + shape);
        }

        // implicit return here, program execution returns to main and continues from where we called testShapes()
    }
}
