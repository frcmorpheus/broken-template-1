package team.frc.morpheus.template1;

/***
 * Example class which implements IShape
 */
public class Circle implements IShape {

    /* Fields of the circle */
    private double x, y, r;

    /* Constructor of the circle. The constructor is like a method, but it has no return type and must have the
     * same name as the class. Code in here will run when someone uses "new" to create a new object of this class.
     */
    public Circle(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public double getRadius() {
        return r;
    }

    /* Below we implement the behaviors required by the IShape interface */

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getArea() {
        return r * r * Math.PI;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + "," + r + ")";
    }
}
